package application;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		
		try {
			Locale.setDefault(Locale.US);
			Scanner sc = new Scanner(System.in);
			System.out.print("Digite a primeira nota: ");
			double av1 = sc.nextDouble();
			System.out.print("Digite a segunda nota: ");
			double av2 = sc.nextDouble();
			System.out.print("Digite a terceira nota: ");
			double av3 = sc.nextDouble();
			
			if (av1 < 0 || av2 < 0 || av3 < 0) {
				throw new IllegalStateException("A nota n�o pode ser menor que 0!");
			}
			
			System.out.println();
			System.out.println("A m�dia � : " + String.format("%.2f", retornaMedia(av1, av2, av3) / 2));
		}
		catch(InputMismatchException e) {
			System.out.println("Apenas n�meros s�o v�lidos!");
		}
		catch(IllegalStateException e) {
			System.out.println(e.getMessage());
		}

	}
	
	public static double retornaMedia(double av1, double av2, double av3) {
		double maior1 = av1;
		double maior2 = av2;
		
		if (av3 > maior1) {
			maior1 = av3;
		} else if (av3 > maior2) {
			maior2 = av3;
		}
		
		return (maior1 + maior2) / 2;
	}

}
